import pickle
import socket
import math
import gopigo as go
import tic_tac_toe_logic as logic
import numpy as np
import time

CAMERA_PORT = 12320
CAMERA_HOST = '172.17.202.162'

gameSquares = {0: [199, 109], 1: [352, 107], 2: [506, 98],
               3: [194, 249], 4: [352, 250], 5: [506, 243],
               6: [191, 387], 7: [354, 386], 8: [509, 383],
               9: [0, 0], 10: [0, 0]}

camera_socket = socket.socket()


##########
# Camera #
##########

def connect_to_camera():
    camera_socket.connect((CAMERA_HOST, CAMERA_PORT))


def disconnect_from_camera():
    camera_socket.send(pickle.dumps('q'))
    camera_socket.close()


def get_coords():
    camera_socket.send(pickle.dumps('c'))
    coords = pickle.loads(camera_socket.recv(1024))
    #print("Received data : ", coords)
    return coords


def get_game_square_coords():
    camera_socket.send(pickle.dumps('f'))
    received_data = pickle.loads(camera_socket.recv(1024))
    print("Received data : ", received_data)
    for i in range(0, 9):
        gameSquares[i][0] = received_data[i][0]
        gameSquares[i][1] = received_data[i][1]
      
    gameSquares[9][0] = (gameSquares[3][0] + gameSquares[6][0]) / 2
    gameSquares[9][1] = (gameSquares[3][1] + gameSquares[6][1]) / 2 - 30
 
    #gameSquares[10][0] = (gameSquares[3][0] + gameSquares[0][0]) / 2
    #gameSquares[10][1] = (gameSquares[3][1] + gameSquares[0][1]) / 2
    gameSquares[10][0] = (gameSquares[3][0] + gameSquares[6][0]) / 2
    gameSquares[10][1] = (gameSquares[3][1] + gameSquares[6][1]) / 2 + 10

    print("Game squares coords updated")


########
# Math #
########

def is_point_on_line(x1, y1, x2, y2, square_index):
    const = 0.15

    square_x = gameSquares[square_index][0]
    square_y = gameSquares[square_index][1]

    left_value = (square_x - x1) / (x2 - x1)
    right_value = (square_y - y1) / (y2 - y1)

    return left_value - const <= right_value <= left_value + const


def check_direction(x1, y1, x2, y2, square_index):
    square_x = gameSquares[square_index][0]
    square_y = gameSquares[square_index][1]

    d1 = math.sqrt((square_x - x1) ** 2 + (square_y - y1) ** 2)
    d2 = math.sqrt((square_x - x2) ** 2 + (square_y - y2) ** 2)

    return d1 < d2


def calc_angle(s_x, s_y, x1, y1, x2, y2):
    vect_a = [s_x - x2, s_y - y2]
    vect_b = [x1 - x2, y1 - y2]

    angle_a = np.arctan2(vect_a[0], vect_a[1])
    angle_b = np.arctan2(vect_b[0], vect_b[1])
    
    angle_dir = int(np.rad2deg(angle_a - angle_b))
    
    #print("Angle from setpoint:", angle_dir)
    
    return angle_dir


def get_rotate_direction(s_x, s_y, x1, y1, x2, y2):
    # Calculate vector coordinates (start = back marker, end = front marker/setpoint)
    vect_a = [s_x - x2, s_y - y2]
    vect_b = [x1 - x2, y1 - y2]

    # Calculate cross product to choose which way to turn
    return np.cross(vect_a, vect_b)


############
# Movement #
############

def drive_to_point(square_index, target_distance):
    k_p = 0.5
    max_speed = 60

    print("Driving to square: ", square_index)
    square_x = gameSquares[square_index][0]
    square_y = gameSquares[square_index][1]

    distance = 1000

    # drive until at the right coordinates (+-)
    while distance > target_distance - 15 or distance < (target_distance - 60):
        
        robot_coords = get_coords()
        
        if len(robot_coords[0]) != 2 or len(robot_coords[1]) != 2:
            print("Missing some coordinates, asking again")
            
        else:
            # Robot front marker coords
            x1 = robot_coords[0][0]
            y1 = robot_coords[0][1]

            distance = math.sqrt((square_x - x1)**2 + (square_y - y1)**2)
            robot_speed = k_p * distance

            if robot_speed > max_speed:
                robot_speed = max_speed

            #print("Distance from setpoint: ", distance)

            go.set_speed(int(robot_speed))
            
            if distance > target_distance:
                go.fwd()
            else:
                go.bwd()
            

    go.stop()
    print("Arrived on square: ", square_index, " robot front marker: x = ", x1, ", y = ", y1)


def rotate_to_direction(square_index):
    rotating_speed = 40

    print("Rotating to square: ", square_index)

    direction = None

    square_x = gameSquares[square_index][0]
    square_y = gameSquares[square_index][1]
            
    go.set_speed(rotating_speed)
    while True:
        robot_coords = get_coords()

        if len(robot_coords[0]) != 2 or len(robot_coords[1]) != 2:
            print("Missing some coordinates, asking again")

        else:    
            x1 = robot_coords[0][0]
            y1 = robot_coords[0][1]
            x2 = robot_coords[1][0]
            y2 = robot_coords[1][1]

            if (check_direction(x1, y1, x2, y2, square_index) and
                    -5 <= calc_angle(square_x, square_y, x1, y1, x2, y2) <= 5):
                    #is_point_on_line(x1, y1, x2, y2, square_index)):
                print("Direction found, now facing square: ", square_index)
                go.stop()
                return

            #print("Rotating...")

            if direction is None:
                direction = get_rotate_direction(square_x, square_y, x1, y1, x2, y2)

                if direction < 0:
                    go.right_rot()
                elif direction >= 0:
                    go.left_rot()


def drive_to_field():
    go.set_speed(85)
    robot_coords = get_coords()
    
    print("Driving to field")
    while (len(robot_coords[0]) != 2 or len(robot_coords[1]) != 2):
        robot_coords = get_coords()
        go.fwd()
    time.sleep(0.35)
    go.stop()


def draw_circle():
    go.set_speed(40)
    go.fwd()
    time.sleep(0.4)
    
    go.set_left_speed(0)
    go.set_right_speed(50)
    go.bwd()
    time.sleep(0.5)
    
    go.set_speed(50)
    go.bwd()
    time.sleep(0.2)
    
    go.set_left_speed(0)
    go.set_right_speed(60)
    go.fwd()
    time.sleep(0.4)
    
    
def draw_cross():
    go.set_left_speed(0)
    go.set_right_speed(60)

    go.fwd()
    time.sleep(0.6)
    go.stop()
    time.sleep(0.25)
    go.servo(0)
    
    go.bwd()
    time.sleep(0.3)
    go.stop()
    time.sleep(0.25)
    
    go.set_speed(40)
    go.fwd()
    time.sleep(0.3)
    go.servo(90)
    go.stop()
    time.sleep(0.5)
    
    go.bwd()
    time.sleep(0.7)
    

def draw_mark(mark):
    # TODO implement me
    print("Drawing mark ", mark)
    go.enable_servo()
    
    time.sleep(0.5)

    if (mark == 'o'):
        go.servo(90)
        draw_circle()
    else:
        go.right_rot()
        time.sleep(0.5)
        go.stop()
        go.servo(90)
        draw_cross()
        
    go.stop()  
    go.servo(0)


def drive_off_field(mark):
    line_index = 0
    
    if (mark == 'o'):
        target = 8
        line_index = 10
    else:
        target = 2
        line_index = 9
    
    print("Driving off field")
    rotate_to_direction(target)
    drive_to_point(target, 65)

    rotate_to_direction(line_index)
    
    go.set_speed(60)
    go.bwd()
    time.sleep(3)
           

def stop():
    # For finally block in chip and dale
    go.stop()
    print("Stop movement")


def make_move(square_index, mark):
    go.enable_servo()
    go.servo(0)
    print("Making move to square: ", square_index)
    drive_to_field()
    rotate_to_direction(square_index)
    drive_to_point(square_index, 125)
    rotate_to_direction(square_index)
    draw_mark(mark)
    drive_off_field(mark)
    go.stop()


##########################
# Logic and game control #
##########################

def get_optimal_move(opponent_move, self_mark, opponent_mark):
    my_move = logic.choose_move(self_mark, opponent_mark, opponent_move)
    print("Opponents last move: ", opponent_move)
    print("My next move: ", my_move)
    print("Board after my move")
    logic.print_field()
    return my_move


def is_game_over(marker):
    if logic.is_draw():
        return 0
    if logic.is_winner(marker):
        return 1


def number_of_free_squares():
    return len(logic.empty_squares())


def draw_opponent_move(mark, square_index):
    logic.draw_mark(mark, square_index)


def print_field():
    logic.print_field()


def end_game(result):
    print("Finish with result: ", result)
    if result == 1:
        print("Winner")
    elif result == 0:
        print("It's a tie")
    else:
        print("Loser")
