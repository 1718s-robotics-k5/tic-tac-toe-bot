import gopigo as go
import time

def draw_circle():
    go.set_speed(40)
    go.fwd()
    time.sleep(0.4)
    
    go.set_left_speed(0)
    go.set_right_speed(50)
    go.bwd()
    time.sleep(0.5)
    
    go.set_speed(50)
    go.bwd()
    time.sleep(0.2)
    
    go.set_left_speed(0)
    go.set_right_speed(60)
    go.fwd()
    time.sleep(0.4)
    
    go.stop()
    
def draw_cross():
    go.set_left_speed(0)
    go.set_right_speed(60)

    go.fwd()
    time.sleep(0.6)
    go.stop()
    time.sleep(0.25)
    go.servo(0)
    
    go.bwd()
    time.sleep(0.3)
    go.stop()
    time.sleep(0.25)
    
    go.set_speed(40)
    go.fwd()
    time.sleep(0.3)
    go.servo(90)
    go.stop()
    time.sleep(0.5)
    
    go.bwd()
    time.sleep(0.7)
    
    go.stop()    


go.enable_servo()
go.servo(90)
#time.sleep(1)

draw_circle()

go.servo(0)
#time.sleep(1)

go.stop()