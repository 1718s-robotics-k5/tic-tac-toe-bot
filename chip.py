import random
import socket
import robot

# Client robot

ROBOT_PORT = 12310
ROBOT_HOST = '172.17.203.165'
SELF_MARKER = 'x'
OPPONENT_MARKER = 'o'


def setup_game():
    # init connection to camera
    robot.connect_to_camera()
    robot.get_game_square_coords()
    # draw field
    # init connection to Dale
    global dale
    dale = socket.socket()
    #dale.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    print("Connecting to Dale...")
    dale.connect((ROBOT_HOST, ROBOT_PORT))


def choose_starting_player():
    first_player = random.randint(0, 1)
    # tell Dale who starts
    dale.send(str(first_player).encode())
    if first_player == 1:
        print("Chip starts")
        # Choose random square
        first_move = robot.get_optimal_move(None, SELF_MARKER, OPPONENT_MARKER)
        robot.make_move(first_move, SELF_MARKER)
        dale.send(str(first_move).encode())
    else:
        # Dale starts
        print("Dale starts")


result = None   # None=game not over, -1=lost, 0=tie, 1=win
dale = None

try:
    setup_game()
    choose_starting_player()

    while result is None:
        # wait for opponents move
        print("\n---New Round---")
        opponent_move = int(dale.recv(1024).decode())

        if robot.number_of_free_squares() == 0:
            result = robot.is_game_over(SELF_MARKER)
            break

        elif robot.number_of_free_squares() == 1:
            print("Opponents last move: ", opponent_move)
            robot.draw_opponent_move(OPPONENT_MARKER, opponent_move)
            robot.print_field()
            result = robot.is_game_over(SELF_MARKER)
            break

        else:
            my_move = robot.get_optimal_move(opponent_move, SELF_MARKER, OPPONENT_MARKER)
            # Drive the robot
            robot.make_move(my_move, SELF_MARKER)
            # Check game end conditions
            result = robot.is_game_over(SELF_MARKER)
            # send move
            dale.send(str(my_move).encode())

    robot.end_game(result)

finally:
    robot.stop()
    robot.disconnect_from_camera()
    dale.close()
