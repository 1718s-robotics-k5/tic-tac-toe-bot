#source from robotex 2017 team Super Friends: https://github.com/Avamander/SuperFriends/blob/master/kalibreerimine.py
import cv2

cap = cv2.VideoCapture(0)
hsv = None
punktid = list()


def mouse_callback(event, x, y, flags, params):
    if event == 2:
        punktid.append(hsv[y][x])
    if event == 1:
        print("x: "+str(x)+" y: "+str(y))


cv2.namedWindow('image')
cv2.setMouseCallback('image', mouse_callback)

while True:

    ret, frame = cap.read()
    blurred = cv2.GaussianBlur(frame, (5, 5), 0)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    cv2.imshow('image', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
h = list()
s = list()
v = list()

for i in range(len(punktid)):
    h.append(punktid[i][0])
    s.append(punktid[i][1])
    v.append(punktid[i][2])

parameetrid = [min(h), min(s), min(v), max(h), max(s), max(v)]

while True:
    try:
        select=int(input("select marker. (0=front, 1=back, 2=field): "))
        while (select not in [0, 1, 2]):
            select = int(input("1 or 0. select marker. (0=front, 1=back, 2=field): "))
        break
    except:
        print("enter an integer")

fail = open("values.txt", "r")
lines=fail.readlines()
fail.close()
lines[select]=""
for i in parameetrid:
    lines[select]+=(str(i)+",")
lines[select]+="\n"
fail = open("values.txt", "w")
for i in lines:
    fail.write(i)
fail.close()
