import socket
import pickle
from _thread import *
import numpy as np
import cv2
import time

cap = cv2.VideoCapture(0)
kernel = np.ones((9, 9), np.uint8)

CAMERA_HOST = '172.17.202.162'
CAMERA_PORT = 12320


# read hsv values
f = open("values.txt", "r")
lines = f.readlines()
f.close()
l = 0
for line in lines:
    lines[l] = line.split(",")
    lines[l] = lines[l][:-1]
    l += 1
fronthsv = []
backhsv = []
print(lines)
for i in lines[0]:
    fronthsv.append(int(i))
for i in lines[1]:
    backhsv.append(int(i))

frontLower = np.array(fronthsv[:3])
frontUpper = np.array(fronthsv[3:])
backLower = np.array(backhsv[:3])
backUpper = np.array(backhsv[3:])

# blobdetector params
params = cv2.SimpleBlobDetector_Params()

params.filterByArea = True
params.minArea = 0
params.maxArea = 10000000
params.filterByCircularity = False
params.filterByColor = True
params.filterByConvexity = False
params.filterByInertia = False
params.blobColor = 255
# params.minThreshold = False
# params.maxThreshold = False
# params.thresholdStep = False

detector = cv2.SimpleBlobDetector_create(params)


# return coordinates [x1, y1, x2, y2]
def findRobotCoords(lower, upper):
    # cap = cv2.VideoCapture(0)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        pass
    ret, frame = cap.read()
    im = frame
    while type(frame) == None:
        ret, frame = cap.read()

    # got frame
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    thresholded = cv2.inRange(hsv, lower, upper)
    gb = cv2.GaussianBlur(thresholded, (5, 5), 0)
    closing = cv2.morphologyEx(gb, cv2.MORPH_CLOSE, kernel)
    cv2.imshow("closing", closing)
    keypoints = detector.detect(closing)
    if len(keypoints) > 0 and keypoints[0].size > 5:
        return [keypoints[0].pt[0], keypoints[0].pt[1]]

    return [0]

def findFieldCoords():
    index=3
    if cv2.waitKey(1) & 0xFF == ord('q'):
        pass
    h=0
    w=100
    ret, frame = cap.read()
    while not (h - 5 < w < h + 5):
        ret, frame = cap.read()
        while type(frame) == None:
            ret, frame = cap.read()
            # got frame
        gray=cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        thresh=cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
        #thresholded
        im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        if len(contours)<=index:
            index = len(contours)
        cnt = sorted(contours, key=cv2.contourArea)
        if len(cnt) >= index:
            c = cnt[-index] #change this, -1 is largest contour
        else:
            c = cnt[-len(cnt)-1]
        x, y, w, h = cv2.boundingRect(c)
    # find center
    M = cv2.moments(c)
    cX = int(M["m10"] / M["m00"])
    cY = int(M["m01"] / M["m00"])
    # find diameter
    area = cv2.contourArea(c)
    equi_diameter = int(np.sqrt(4 * area / np.pi))+10
    squareside= equi_diameter
    #the world is not perfect
    #squareside = int(equi_diameter/np.sqrt(2))

    #for drawing the corners
    
    cv2.circle(frame, (cX, cY), 7, (255, 255, 255), -1)
    
    cv2.circle(frame, (cX + equi_diameter, cY - equi_diameter), 7, (255, 255, 255), -1)
    cv2.circle(frame, (cX + equi_diameter, cY + equi_diameter), 7, (255, 255, 255), -1)
    cv2.circle(frame, (cX - equi_diameter, cY - equi_diameter), 7, (255, 255, 255), -1)
    cv2.circle(frame, (cX - equi_diameter, cY + equi_diameter), 7, (255, 255, 255), -1)
    #cv2.circle(frame, (cX, cY), equi_diameter, (255, 255, 255), -1) #show radius
    #equi_diameter=int(equi_diameter/np.sqrt(2))
    cv2.circle(frame, (cX + equi_diameter, cY), 7, (255, 255, 255), -1)
    cv2.circle(frame, (cX - equi_diameter, cY), 7, (255, 255, 255), -1)
    cv2.circle(frame, (cX, cY + equi_diameter), 7, (255, 255, 255), -1)
    cv2.circle(frame, (cX, cY - equi_diameter), 7, (255, 255, 255), -1)
    cv2.drawContours(frame, [c],0,(0,0,255),2)
    cv2.imshow("field detection", frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        pass
    return(np.rint([cX - equi_diameter, cY - equi_diameter]), np.rint([cX, cY - squareside]), np.rint([cX + equi_diameter, cY - equi_diameter]),
           np.rint([cX - squareside, cY]), np.rint([cX, cY]), np.rint([cX + squareside, cY]),
           np.rint([cX - equi_diameter, cY + equi_diameter]), np.rint([cX, cY + squareside]), np.rint([cX + equi_diameter, cY + equi_diameter]))

def format_coords():
    output = []
    output.append(np.rint(findRobotCoords(frontLower, frontUpper)))
    output.append(np.rint(findRobotCoords(backLower, backUpper)))
    return output


def send_coords(coords, client):
    print("Sending data: ",coords)
    client.send(pickle.dumps(coords))


def start_new_client(client):
    try:
        while True:
            request = pickle.loads(client.recv(1024))
            print("Request from client: ", request)

            if request == 'q':
                print("Client closed connection")
                break
            elif request == 'c':
                print("Client requested coordinates")
                output = format_coords()
                send_coords(output, client)
            elif request == 'f':
                print("Client requested field coordinates")
                global field
                send_coords(field, client)
            else:
                print("Unknown request")
    finally:
        client.close()

server_socket = socket.socket()
server_socket.bind((CAMERA_HOST, CAMERA_PORT))

print("Listening...")
server_socket.listen(2)
#find first frame
ret, frame = cap.read()
field = findFieldCoords()
print(field)
try:
    while True:
        c, addr = server_socket.accept()
        print("Client connected, address: ", addr)
        start_new_thread(start_new_client, (c,))
finally:
    server_socket.close()

