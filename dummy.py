import robot

robot.connect_to_camera()
robot.get_game_square_coords()

SELF_MARKER = 'o'

square_index = 8

try:
    #for i in range(0, 9):
    #    print("Index:", i)
    #    robot.make_move(i, SELF_MARKER)
    robot.make_move(5, SELF_MARKER)
    robot.make_move(8, SELF_MARKER)
finally:
    robot.stop()
    robot.disconnect_from_camera()
