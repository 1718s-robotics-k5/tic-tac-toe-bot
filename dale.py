import socket
import robot

# Server robot

ROBOT_PORT = 12310
ROBOT_HOST = '172.17.203.165'
SELF_MARKER = 'o'
OPPONENT_MARKER = 'x'


def setup_game():
    # init connection to camera
    robot.connect_to_camera()
    robot.get_game_square_coords()
    print("Waiting for Chip to connect...")
    sock.listen(1)
    global chip
    chip, addr = sock.accept()


def choose_starting_player():
    first_player = int(chip.recv(1024).decode())
    if first_player == 0:
        print("Dale starts")
        # Choose random square
        first_move = robot.get_optimal_move(None, SELF_MARKER, OPPONENT_MARKER)
        robot.make_move(first_move, SELF_MARKER)
        chip.send(str(first_move).encode())
    else:
        print("Chip starts")
        return None


result = None  # None=game not over, -1=lost, 0=tie, 1=win

sock = socket.socket()
# Allow reuse of port and address combo (on windows), set SO_REUSE flag true(1) in SOL_SOCKET
# SOL_SOCKET - socket layer itself
# SO_REUSEADDR - on windows = SO_REUSEADDR + SO_REUSEPORT
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind((ROBOT_HOST, ROBOT_PORT))

try:
    setup_game()
    choose_starting_player()

    while result is None:
        print("\n---New Round---")

        # Wait for opponents move
        opponent_move = int(chip.recv(1024).decode())

        if robot.number_of_free_squares() == 0:
            # Check game end conditions
            result = robot.is_game_over(SELF_MARKER)
            break

        elif robot.number_of_free_squares() == 1:
            print("Opponents last move: ", opponent_move)
            robot.draw_opponent_move(OPPONENT_MARKER, opponent_move)
            robot.print_field()
            # Check game end conditions
            result = robot.is_game_over(SELF_MARKER)
            break

        else:
            my_move = robot.get_optimal_move(opponent_move, SELF_MARKER, OPPONENT_MARKER)
            # Drive the robot
            robot.make_move(my_move, SELF_MARKER)
            # Check game end conditions
            result = robot.is_game_over(SELF_MARKER)
            # Send move to opponent
            chip.send(str(my_move).encode())

    robot.end_game(result)
finally:
    robot.stop()
    robot.disconnect_from_camera()
    chip.close()
    sock.close()

