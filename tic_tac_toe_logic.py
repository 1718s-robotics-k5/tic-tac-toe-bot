import random

field = [' ', ' ', ' ',
         ' ', ' ', ' ',
         ' ', ' ', ' ']


# add move to field
def draw_mark(mark, location):
    field[location] = mark


# check if winner after last move
def is_winner(mark):
    return field[0] == mark and field[1] == mark and field[2] == mark or \
           field[3] == mark and field[4] == mark and field[5] == mark or \
           field[6] == mark and field[7] == mark and field[8] == mark or \
           field[0] == mark and field[3] == mark and field[6] == mark or \
           field[1] == mark and field[4] == mark and field[7] == mark or \
           field[2] == mark and field[5] == mark and field[8] == mark or \
           field[0] == mark and field[4] == mark and field[8] == mark or \
           field[2] == mark and field[4] == mark and field[6] == mark


# check if draw after last move
def is_draw():
    full = 0
    for i in range(len(field)):
        if field[i] != ' ':
            full += 1
    return full == 9


# check middle
def middle_empty():
    return field[4] == ' '


# check if two in a row to win
def two_row(mark):
    if field[0] == ' ':
        if field[1] == mark and field[2] == mark or \
                field[3] == mark and field[6] == mark or \
                field[4] == mark and field[8] == mark:
            return 0

    if field[2] == ' ':
        if field[0] == mark and field[1] == mark or \
                field[4] == mark and field[6] == mark or \
                field[5] == mark and field[8] == mark:
            return 2

    if field[6] == ' ':
        if field[0] == mark and field[3] == mark or \
                field[4] == mark and field[2] == mark or \
                field[7] == mark and field[8] == mark:
            return 6

    if field[8] == ' ':
        if field[6] == mark and field[7] == mark or \
                field[4] == mark and field[0] == mark or \
                field[2] == mark and field[5] == mark:
            return 8

    if field[1] == ' ':
        if field[0] == mark and field[2] == mark or \
                field[4] == mark and field[7] == mark:
            return 1

    if field[3] == ' ':
        if field[0] == mark and field[6] == mark or \
                field[4] == mark and field[5] == mark:
            return 3

    if field[5] == ' ':
        if field[3] == mark and field[4] == mark or \
                field[2] == mark and field[8] == mark:
            return 5

    if field[7] == ' ':
        if field[1] == mark and field[4] == mark or \
                field[6] == mark and field[8] == mark:
            return 7
    return -1


# find the best corners 
def corner_empty(mark):
    empty_corners = []
    corners = [0, 2, 6, 8]

    self_squares = []
    for i in range(len(field)):
        if field[i] == mark:
            self_squares.append(i)

    if 4 not in self_squares:
        if 0 in self_squares:
            corners.remove(8)
        elif 1 in self_squares:
            corners.remove(6)
            corners.remove(8)
        elif 2 in self_squares:
            corners.remove(6)
        elif 3 in self_squares:
            corners.remove(2)
            corners.remove(8)
        elif 5 in self_squares:
            corners.remove(0)
            corners.remove(6)
        elif 6 in self_squares:
            corners.remove(2)
        elif 7 in self_squares:
            corners.remove(0)
            corners.remove(2)
        elif 8 in self_squares:
            corners.remove(0)

    for i in corners:
        if field[i] == ' ':
            empty_corners.append(i)

    return empty_corners


#  find all of the empty squares
def empty_squares():
    empty = []
    for i in range(len(field)):
        if field[i] == ' ':
            empty.append(i)
    return empty


def print_field():
    out = ""
    for i in range(1, 10):
        if i % 3 == 0:
            out += str(field[i - 1])
            out += '\n'
        elif (i+3) % 3 == 2:
            out += '|'
            out += str(field[i - 1])
            out += '|'
        else:
            out += str(field[i - 1])

    print(out)


# chooses where to move
def choose_move(self, opponent, opponent_move):
    # add other player's move to own field
    if opponent_move is None:
        square = random.randint(0, 8)
        draw_mark(self, square)
    else:
        draw_mark(opponent, opponent_move)
        # if middle empty, draw in the middle
        if middle_empty():
            square = 4
            draw_mark(self, square)

        else:
            two_row_self = two_row(self)

            # if you have two, try to win
            if two_row_self != -1:
                square = two_row_self
                draw_mark(self, square)

            else:
                two_row_opponent = two_row(opponent)

                # if opponent has two, don't let them win
                if two_row_opponent != -1:
                    square = two_row_opponent
                    draw_mark(self, square)

                else:
                    empty_corners = corner_empty(self)

                    # draw in a random empty corner
                    if len(empty_corners) != 0:
                        square = random.choice(empty_corners)
                        draw_mark(self, square)

                    # else draw in a random empty square
                    else:
                        square = random.choice(empty_squares())
                        draw_mark(self, square)

    return square

